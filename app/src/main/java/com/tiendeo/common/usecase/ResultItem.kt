package com.tiendeo.common.usecase

interface ResultItem<T> : Result<T> {
    fun success(item: T)
}