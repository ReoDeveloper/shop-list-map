package com.tiendeo.common.usecase

import com.tiendeo.common.Repository


abstract class UseCase<T>(val repository: Repository<T>) {
    abstract fun execute(callback: Result<T>)
}
