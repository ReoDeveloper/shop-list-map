package com.tiendeo.common.usecase

interface Result<T> {
    fun error(message:String)
}