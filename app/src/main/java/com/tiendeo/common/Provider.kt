package com.tiendeo.common

import android.content.Context
import com.tiendeo.interviewtest.data.Mapper
import com.tiendeo.interviewtest.data.ShopRepository
import com.tiendeo.interviewtest.data.SpecificationById
import com.tiendeo.interviewtest.data.TwoWayMapper
import com.tiendeo.interviewtest.data.retrofit.ApiShop
import com.tiendeo.interviewtest.data.retrofit.RetrofitDataSource
import com.tiendeo.interviewtest.data.retrofit.RfShopMapper
import com.tiendeo.interviewtest.data.room.DbShop
import com.tiendeo.interviewtest.data.room.DbShopMapper
import com.tiendeo.interviewtest.data.room.RoomDataSource
import com.tiendeo.interviewtest.domain.model.Shop
import com.tiendeo.interviewtest.domain.usecases.GetAllShops
import com.tiendeo.interviewtest.domain.usecases.GetShopById

class UseCaseProvider {
    companion object {
        fun provideGetAllShopsUseCase(context: Context): GetAllShops =
                GetAllShops(RepositoryProvider.provideShopRepository(context))
        fun provideGetShopById(context: Context, idShop:Long):GetShopById =
                GetShopById(RepositoryProvider.provideShopRepository(context), SpecificationById(idShop))
    }
}

class RepositoryProvider {
    companion object {
        fun provideShopRepository(context: Context): Repository<Shop> =
                ShopRepository(DataSourceProvider.provideShopRetrofitDataSource())
                        .apply { cache = DataSourceProvider.provideShopRoomDataSource(context) }
    }
}

class DataSourceProvider {
    companion object {
        fun provideShopRetrofitDataSource(): DataSource<Shop> =
                RetrofitDataSource(MapperProvider.provideApiShopMapper())
        fun provideShopRoomDataSource(context: Context): DataSource<Shop> =
                RoomDataSource(context, MapperProvider.provideDbShopMapper())
    }
}

class MapperProvider {
    companion object {
        fun provideApiShopMapper(): Mapper<ApiShop, Shop> = RfShopMapper()
        fun provideDbShopMapper(): TwoWayMapper<DbShop, Shop> = DbShopMapper()
    }
}