package com.tiendeo.interviewtest.ui.shops_list

import android.support.v4.app.Fragment

data class ShopsFragment(val title:String, val content:Fragment)