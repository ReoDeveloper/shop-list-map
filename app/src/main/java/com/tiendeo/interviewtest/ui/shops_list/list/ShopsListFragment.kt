package com.tiendeo.interviewtest.ui.shops_list.list

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tiendeo.common.UseCaseProvider
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.domain.model.Shop
import com.tiendeo.interviewtest.ui.shop_detail.ShopDetailActivity
import com.tiendeo.interviewtest.ui.inflate
import kotlinx.android.synthetic.main.fragment_shops_list.*

class ShopsListFragment : Fragment(), ShopsListContract.View {

    lateinit var presenter: ShopsListContract.Actions

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        presenter = ShopsListPresenter(this, UseCaseProvider.provideGetAllShopsUseCase(requireContext()))
        return container?.inflate(R.layout.fragment_shops_list)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.init()
    }

    override fun showError(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun displayItems(items: List<Shop>) {
        shops_view.layoutManager = GridLayoutManager(activity, 2)
        shops_view.adapter = ShopsListAdapter(items) { shop: Shop, view: View -> presenter.onItemTap(shop, view) }
    }

    override fun goToDetail(id: Long, view: View) {
        ShopDetailActivity.startActivity(requireContext(), id, view)
    }

    override fun showLoading() {
        // TODO: How to inform the user that we are loading?
    }

    override fun hideLoading() {
        // TODO: How to inform the user that we are loading?
    }
}
