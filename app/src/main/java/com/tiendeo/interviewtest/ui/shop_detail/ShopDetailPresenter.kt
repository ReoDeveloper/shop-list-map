package com.tiendeo.interviewtest.ui.shop_detail

import com.tiendeo.common.usecase.Executor
import com.tiendeo.common.usecase.ResultItem
import com.tiendeo.interviewtest.domain.model.Shop
import com.tiendeo.interviewtest.domain.usecases.GetShopById

class ShopDetailPresenter(val view: ShopDetailContract.View, private val usecase: GetShopById) : ShopDetailContract.Actions {

    private val executor: Executor = Executor.getInstance()

    override fun init(idShop: Long) {
        if (idShop < 0) {
            view.showError("Invalid idShop [$idShop] received")
            return
        }
        view.showLoading()
        executor.execute(usecase, object : ResultItem<Shop> {
            override fun error(message: String) {
                view.showError(message)
                view.hideLoading()
            }

            override fun success(item: Shop) {
                with(item) {
                    view.displayName(shopName)
                    view.displayImage("https://picsum.photos/300?image=" + (shopId % 1000))
                    view.displayCity(addressCity)
                    view.displayStreet(addressStreet)
                    view.displayPostal(addressPostalCode)
                    view.displayLocation(latitude, longitude)
                    when (phoneNumber.isEmpty()) {
                        true -> view.hidePhone()
                        else -> view.displayPhone(phoneNumber)
                    }
                    when (web.isEmpty()) {
                        true -> view.hideWeb()
                        else -> view.displayWeb(web)
                    }
                }
                view.hideLoading()
            }
        })
    }
}