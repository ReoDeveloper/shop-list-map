package com.tiendeo.interviewtest.ui.shops_list.list

import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.domain.model.Shop
import com.tiendeo.interviewtest.ui.inflate
import com.tiendeo.interviewtest.ui.loadUrl
import kotlinx.android.synthetic.main.item_shop.view.*

class ShopsListAdapter(val shops: List<Shop>, private val listener: (Shop, View) -> Unit) : RecyclerView.Adapter<ShopsListAdapter.ShopViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ShopViewHolder(parent.inflate(R.layout.item_shop))

    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) {
        holder.bind(shops[position], listener)
    }

    override fun getItemCount() = shops.size

    class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Shop, listener: (Shop, View) -> Unit) = with(itemView) {
            ViewCompat.setTransitionName(img_avatar, item.shopId.toString())
                img_avatar.loadUrl("https://picsum.photos/300?image=" + (item.shopId % 1000))
                shop_name_view.text = item.shopName
                shop_address_view.text =
                        String.format(
                                resources.getString(R.string.txt_address),
                                item.addressStreet,
                                item.addressCity,
                                item.addressPostalCode
                        )
                setOnClickListener { listener(item, img_avatar) }
        }
    }
}
