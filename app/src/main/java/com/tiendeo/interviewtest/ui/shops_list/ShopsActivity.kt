package com.tiendeo.interviewtest.ui.shops_list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.ui.shops_list.list.ShopsListFragment
import com.tiendeo.interviewtest.ui.shops_list.map.ShopsMapFragment
import kotlinx.android.synthetic.main.activity_shops.*

class ShopsActivity : AppCompatActivity() {

    private lateinit var pagerAdapter: ShopsPagerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shops)

        pagerAdapter = ShopsPagerAdapter(supportFragmentManager)
        pagerAdapter.addPage(getString(R.string.txt_list_title), ShopsListFragment())
        pagerAdapter.addPage(getString(R.string.txt_map_title), ShopsMapFragment())

        setupToolbar()
        setupTabs()
    }

    private fun setupTabs() {
        pager.adapter = pagerAdapter
        tabs.setupWithViewPager(pager)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
    }
}
