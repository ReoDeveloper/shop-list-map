package com.tiendeo.interviewtest.ui.shops_list

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ShopsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private var pages : MutableList<ShopsFragment> = ArrayList()

    fun addPage(title:String, content: Fragment){
        pages.add(ShopsFragment(title, content))
    }

    override fun getItem(position: Int) = pages[position].content

    override fun getCount()= pages.size

    override fun getPageTitle(position: Int): CharSequence? = pages[position].title
}
