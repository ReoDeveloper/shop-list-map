package com.tiendeo.interviewtest.ui.shops_list.map

import com.tiendeo.common.usecase.Executor
import com.tiendeo.common.usecase.ResultList
import com.tiendeo.interviewtest.domain.model.Shop
import com.tiendeo.interviewtest.domain.usecases.GetAllShops

class ShopsMapPresenter(val view: ShopsMapContract.View, private val usecase: GetAllShops) : ShopsMapContract.Actions {

    private val executor = Executor.getInstance()
    private val callback = object : ResultList<Shop> {
        override fun success(items: List<Shop>) {
            view.displayItems(items)
            view.hideLoading()
        }

        override fun error(message: String) {
            view.showError(message)
            view.hideLoading()
        }
    }

    override fun mapLoaded() {
        view.moveToDefault()
        view.showLoading()
        executor.execute(usecase, callback)
    }

}