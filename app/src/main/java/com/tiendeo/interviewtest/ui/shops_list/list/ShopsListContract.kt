package com.tiendeo.interviewtest.ui.shops_list.list

import com.tiendeo.interviewtest.domain.model.Shop

interface ShopsListContract {
    interface View {
        fun showError(message: String)
        fun displayItems(items: List<Shop>)
        fun goToDetail(id: Long, view:android.view.View)
        fun showLoading()
        fun hideLoading()
    }

    interface Actions {
        fun init()
        fun onItemTap(item:Shop, view:android.view.View)
    }
}