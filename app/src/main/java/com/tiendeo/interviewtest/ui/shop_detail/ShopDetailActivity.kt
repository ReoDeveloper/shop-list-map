package com.tiendeo.interviewtest.ui.shop_detail

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.tiendeo.common.UseCaseProvider
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.ui.loadUrl
import kotlinx.android.synthetic.main.activity_shop_detail.*
import kotlinx.android.synthetic.main.shop_details.*


class ShopDetailActivity : AppCompatActivity(), ShopDetailContract.View {

    companion object {
        const val ID_SHOP = "idShop"
        const val TRANSITION_NAME = "transition"

        fun startActivity(context: Context, idShop: Long, shared: View? = null) {
            val intent = Intent(context, ShopDetailActivity::class.java).apply {
                putExtra(ID_SHOP, idShop)
            }
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP && shared != null) {
                val options: ActivityOptions = ActivityOptions.makeSceneTransitionAnimation(
                        context as Activity,
                        shared,
                        ViewCompat.getTransitionName(shared))
                intent.apply {
                    putExtra(TRANSITION_NAME, ViewCompat.getTransitionName(shared))
                }
                context.startActivity(intent, options.toBundle())
            } else {
                context.startActivity(intent)
            }
        }
    }

    lateinit var presenter: ShopDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop_detail)
        setupToolbar()
        val idShop = intent.getLongExtra(ID_SHOP, -1)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val imageTransitionName = intent?.extras?.getString(TRANSITION_NAME)
            img_avatar.transitionName = imageTransitionName
        }

        presenter = ShopDetailPresenter(this, UseCaseProvider.provideGetShopById(this, idShop))
        presenter.init(idShop)
    }

    fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.run {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun displayName(value: String) {
        collapsing_toolbar_layout.title = value
    }

    override fun displayImage(value: String) {
        img_avatar.loadUrl(value)
    }

    override fun displayLocation(lat: Double, lon: Double) {
        // TODO: Show on a small map?
    }

    override fun displayStreet(value: String) {
        txt_shop_address.text = value
    }

    override fun displayCity(value: String) {
        txt_shop_city.text = value
    }

    override fun displayPostal(value: String) {
    }

    override fun displayPhone(value: String) {
        lay_shop_phone.visibility = View.VISIBLE
        txt_shop_phone.text = value
    }

    override fun hidePhone() {
        lay_shop_phone.visibility = View.GONE
    }

    override fun displayWeb(value: String) {
        lay_shop_web.visibility = View.VISIBLE
        txt_shop_web.text = value
    }

    override fun hideWeb() {
        lay_shop_web.visibility = View.GONE
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        // TODO: How to inform the user that we are loading?
    }

    override fun hideLoading() {
        // TODO: How to inform the user that we are loading?
    }
}