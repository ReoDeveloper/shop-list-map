package com.tiendeo.interviewtest.ui.shops_list.list

import android.view.View
import com.tiendeo.common.usecase.Executor
import com.tiendeo.common.usecase.ResultList
import com.tiendeo.interviewtest.domain.model.Shop
import com.tiendeo.interviewtest.domain.usecases.GetAllShops

class ShopsListPresenter(val view: ShopsListContract.View, private val usecase: GetAllShops) : ShopsListContract.Actions {

    private val executor = Executor.getInstance()
    private val callback = object : ResultList<Shop> {
        override fun success(items: List<Shop>) {
            view.displayItems(items)
            view.hideLoading()
        }

        override fun error(message: String) {
            view.showError(message)
            view.hideLoading()
        }
    }

    override fun init() {
        view.showLoading()
        executor.execute(usecase, callback)
    }

    override fun onItemTap(item: Shop, sharedView: View) {
        view.goToDetail(item.shopId, sharedView)
    }
}