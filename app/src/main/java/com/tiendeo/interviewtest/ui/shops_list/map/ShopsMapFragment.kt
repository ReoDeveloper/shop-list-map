package com.tiendeo.interviewtest.ui.shops_list.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tiendeo.common.UseCaseProvider
import com.tiendeo.interviewtest.domain.model.Shop
import com.tiendeo.interviewtest.ui.shop_detail.ShopDetailActivity

class ShopsMapFragment : SupportMapFragment(), ShopsMapContract.View {
    companion object {
        private val TIENDEO_LATLNG = LatLng(41.380968, 2.185584)
        private val TIENDEO_NAME = "Tiendeo Office"
        private val ZOOM_DEFAULT = 16f
    }

    private lateinit var map: GoogleMap
    private lateinit var presenter: ShopsMapPresenter

    val callback = OnMapReadyCallback { googleMap ->
        map = googleMap
        presenter.mapLoaded()
    }


    override fun onCreateView(layoutInflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View? {
        val view = super.onCreateView(layoutInflater, container, bundle)
        getMapAsync(callback)
        presenter = ShopsMapPresenter(this, UseCaseProvider.provideGetAllShopsUseCase(requireContext()))
        return view
    }

    override fun showError(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun displayItems(items: List<Shop>) {
        items.forEach {
            map.addMarker(MarkerOptions()
                    .position(LatLng(it.latitude, it.longitude))
                    .title(it.retailerName)
                    .snippet(it.addressStreet)).tag = it.shopId
        }
        map.setOnInfoWindowClickListener {
            ShopDetailActivity.startActivity(requireContext(), it.tag as Long)
        }
    }

    override fun moveToDefault() {
        map.addMarker(MarkerOptions().position(TIENDEO_LATLNG).title(TIENDEO_NAME))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(TIENDEO_LATLNG, ZOOM_DEFAULT))
    }

    override fun showLoading() {
        // TODO: How to inform the user that we are loading?
    }

    override fun hideLoading() {
        // TODO: How to inform the user that we are loading?
    }
}
