package com.tiendeo.interviewtest.ui.shop_detail

interface ShopDetailContract {
    interface View {
        fun displayName(value: String)
        fun displayImage(value: String)
        fun displayLocation(lat: Double, lon: Double)
        fun displayStreet(value: String)
        fun displayCity(value: String)
        fun displayPostal(value: String)
        fun displayPhone(value: String)
        fun hidePhone()
        fun displayWeb(value: String)
        fun hideWeb()
        fun showError(message: String)
        fun showLoading()
        fun hideLoading()
    }

    interface Actions {
        fun init(idShop: Long)
    }
}