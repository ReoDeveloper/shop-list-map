package com.tiendeo.interviewtest.ui.shops_list.map

import com.tiendeo.interviewtest.domain.model.Shop

interface ShopsMapContract {
    interface View {
        fun showError(message: String)
        fun displayItems(items: List<Shop>)
        fun moveToDefault()
        fun showLoading()
        fun hideLoading()
    }

    interface Actions {
        fun mapLoaded()
    }
}