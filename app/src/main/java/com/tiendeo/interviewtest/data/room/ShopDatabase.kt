package com.tiendeo.interviewtest.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(DbShop::class), version = 1)
abstract class ShopDatabase : RoomDatabase() {

    abstract fun shopsDao(): ShopsDao

    companion object {
        private var INSTANCE: ShopDatabase? = null
        private val DB_NAME = "tiendeo"

        fun getInstance(context: Context): ShopDatabase {
            if (INSTANCE == null) {
                synchronized(ShopDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            ShopDatabase::class.java,
                            DB_NAME)
                            .build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}