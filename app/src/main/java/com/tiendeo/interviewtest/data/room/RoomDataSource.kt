package com.tiendeo.interviewtest.data.room

import android.content.Context
import com.tiendeo.common.DataSource
import com.tiendeo.interviewtest.data.Specification
import com.tiendeo.interviewtest.data.SpecificationById
import com.tiendeo.interviewtest.data.TwoWayMapper
import com.tiendeo.interviewtest.domain.model.Shop
import java.lang.IllegalArgumentException

class RoomDataSource(context: Context, val mapper: TwoWayMapper<DbShop, Shop>) : DataSource<Shop> {

    var database: ShopDatabase = ShopDatabase.getInstance(context)

    override fun store(item: Shop) {
        database.shopsDao().insert(mapper.reverseMap(item))
    }

    override fun store(items: List<Shop>) {
        items.map { store(it) }
    }

    override fun getAll(): List<Shop> {
        return mapper.map(database.shopsDao().getAll())
    }

    override fun queryList(specification: Specification): List<Shop> {
        // TODO: Not yet implemented
        return emptyList()
    }

    override fun queryItem(specification: Specification): Shop {
        return when (specification) {
            is SpecificationById ->  mapper.map(database.shopsDao().queryItemById(specification.id))
            else -> throw IllegalArgumentException("The Specification provided is not supported by this DataSource")
        }
    }
}