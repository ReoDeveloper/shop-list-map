package com.tiendeo.interviewtest.data.retrofit

data class ApiShop(
        val shop_id:String,
        val shop_name:String,
        val latitude:String,
        val longitude:String,
        val address_street:String,
        val address_city:String,
        val address_postal_code:String,
        val retailer_id:String,
        val retailer_name:String,
        val has_catalogs:Boolean,
        val phone_number:String,
        val web:String)
