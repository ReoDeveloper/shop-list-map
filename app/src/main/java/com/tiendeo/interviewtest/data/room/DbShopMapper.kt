package com.tiendeo.interviewtest.data.room

import com.tiendeo.interviewtest.data.TwoWayMapper
import com.tiendeo.interviewtest.domain.model.Shop

class DbShopMapper : TwoWayMapper<DbShop, Shop>() {
    override fun map(item: DbShop): Shop {
        item.run {
            return Shop(
                    id,
                    name,
                    latitude,
                    longitude,
                    street,
                    city,
                    postal,
                    retailerId,
                    retailerName,
                    hasCatalogs,
                    phone,
                    web
            )
        }
    }

    override fun reverseMap(item: Shop): DbShop {
        item.run {
            return DbShop(
                    shopId,
                    shopName,
                    latitude,
                    longitude,
                    addressStreet,
                    addressCity,
                    addressPostalCode,
                    retailerId,
                    retailerName,
                    isHasCatalogs,
                    phoneNumber,
                    web
            )
        }
    }
}