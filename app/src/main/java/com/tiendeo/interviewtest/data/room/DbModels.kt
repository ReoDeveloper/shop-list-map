package com.tiendeo.interviewtest.data.room

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "shopsTable")
data class DbShop(@PrimaryKey val id:Long, var name: String, var latitude: Double,
                  var longitude: Double, var street: String, var city: String,
                  var postal: String, var retailerId: Long, var retailerName: String,
                  var hasCatalogs: Boolean, var phone: String, var web: String)