package com.tiendeo.interviewtest.data.retrofit

import com.tiendeo.common.DataSource
import com.tiendeo.interviewtest.data.Mapper
import com.tiendeo.interviewtest.data.Specification
import com.tiendeo.interviewtest.domain.model.Shop

class RetrofitDataSource(val mapper: Mapper<ApiShop, Shop>, url: String = BASE_URL) : DataSource<Shop> {

    companion object {
        val BASE_URL = "https://interview-test-45073.firebaseio.com/"
    }

    val emptyShopItem = Shop(0L, "", 0.0, 0.0, "",
            "", "", 0L, "", false,
            "", "")
    val apiService: RetrofitService = RetrofitClient.getClient(url)!!.create(RetrofitService::class.java)

    override fun store(item: Shop) {
        // This is method is not supported for the api
        throw UnsupportedOperationException("This Datasource has no storing capabilities")
    }

    override fun store(items: List<Shop>) {
        // This is method is not supported for the api
        throw UnsupportedOperationException("This Datasource has no storing capabilities")
    }

    override fun getAll(): List<Shop> {
        val call = apiService.getShops()
        val callResponse = call.execute()
        if (callResponse?.isSuccessful!!) {
            val apiShopResponse = callResponse.body()
            apiShopResponse?.let {
                return mapper.map(it)
            }
        }
        return emptyList()
    }

    override fun queryList(specification: Specification): List<Shop> {
        // TODO: We do not need query capabilities (yet)
        return emptyList()
    }

    override fun queryItem(specification: Specification): Shop {
        // TODO: We do not need query capabilities (yet)
        return emptyShopItem
    }
}