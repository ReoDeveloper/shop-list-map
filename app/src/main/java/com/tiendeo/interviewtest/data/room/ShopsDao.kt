package com.tiendeo.interviewtest.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

@Dao
interface ShopsDao {
    @Query("SELECT * from shopsTable")
    fun getAll(): List<DbShop>

    @Query("SELECT * from shopsTable WHERE id = :idShop")
    fun queryItemById(idShop:Long):DbShop

    @Insert(onConflict = REPLACE)
    fun insert(item: DbShop)
}