package com.tiendeo.interviewtest.data.retrofit

import retrofit2.Call
import retrofit2.http.GET

interface RetrofitService {
    @GET("shops.json")
    fun getShops(): Call<List<ApiShop>>
}
