package com.tiendeo.interviewtest.data

// This provides a common ancestor
interface Specification {
    // To be defined on the children
}

data class SpecificationById(val id: Long) : Specification