package com.tiendeo.interviewtest.data

import com.tiendeo.common.DataSource
import com.tiendeo.common.Repository
import com.tiendeo.interviewtest.domain.model.Shop

class ShopRepository(dataSource: DataSource<Shop>) : Repository<Shop>(dataSource) {
    var cache: DataSource<Shop>? = null

    override fun store(item: Shop) {
        cache?.store(item)
    }

    override fun store(items: List<Shop>) {
        cache?.store(items)
    }

    override fun queryItem(specification: Specification): Shop {
        cache?.run {
            queryItem(specification).also { return it }
        }
        return super.queryItem(specification)
    }

    override fun getAll(): List<Shop> {
        cache?.getAll()?.run { if(isNotEmpty()) return this }
        // If we have to resort to an api call, make sure to store it afterwards
        return super.getAll().also { cache?.store(it) }
    }

}