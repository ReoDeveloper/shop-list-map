package com.tiendeo.interviewtest.data.retrofit

import com.tiendeo.interviewtest.data.Mapper
import com.tiendeo.interviewtest.domain.model.Shop

class RfShopMapper : Mapper<ApiShop, Shop>() {

    override fun map(item: ApiShop): Shop {
        return Shop(
                item.shop_id.toLong(),
                item.shop_name,
                item.latitude.replace(",", ".").toDouble(),
                item.longitude.replace(",", ".").toDouble(),
                item.address_street,
                item.address_city,
                item.address_postal_code,
                item.retailer_id.toLong(),
                item.retailer_name,
                item.has_catalogs,
                item.phone_number,
                item.web)
    }
}