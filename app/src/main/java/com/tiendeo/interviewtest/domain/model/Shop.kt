package com.tiendeo.interviewtest.domain.model

data class Shop(
        var shopId: Long,
        var shopName: String,
        var latitude: Double,
        var longitude: Double,
        var addressStreet: String,
        var addressCity: String,
        var addressPostalCode: String,
        var retailerId: Long,
        var retailerName: String,
        var isHasCatalogs: Boolean,
        var phoneNumber: String,
        var web: String
)
