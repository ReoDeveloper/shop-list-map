package com.tiendeo.interviewtest.domain.usecases

import android.os.Handler
import android.os.Looper
import com.tiendeo.common.Repository
import com.tiendeo.common.usecase.Result
import com.tiendeo.common.usecase.ResultList
import com.tiendeo.common.usecase.UseCase
import com.tiendeo.interviewtest.domain.model.Shop

class GetAllShops(repository: Repository<Shop>):UseCase<Shop>(repository) {

    override fun execute(callback: Result<Shop>) {
        val result = repository.getAll()
        val runnable = Runnable {
            if(callback is ResultList<Shop>){
                callback.success(result)
            }else{
                callback.error("Wrong callback. Result is expected to be a List")
            }
        }
        Handler(Looper.getMainLooper()).post(runnable)
    }

}