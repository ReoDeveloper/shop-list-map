package com.tiendeo.interviewtest.domain.usecases

import android.os.Looper
import android.os.Handler
import com.tiendeo.common.Repository
import com.tiendeo.common.usecase.Result
import com.tiendeo.common.usecase.ResultItem
import com.tiendeo.common.usecase.UseCase
import com.tiendeo.interviewtest.data.Specification
import com.tiendeo.interviewtest.domain.model.Shop

class GetShopById(repository: Repository<Shop>, private val specification: Specification) : UseCase<Shop>(repository) {

    override fun execute(callback: Result<Shop>) {
        val result = repository.queryItem(specification)
        val runnable = Runnable {
            if(callback is ResultItem<Shop>){
                callback.success(result)
            }else{
                callback.error("Wrong callback. Result must be returned as an item")
            }
        }
        Handler(Looper.getMainLooper()).post(runnable)
    }
}