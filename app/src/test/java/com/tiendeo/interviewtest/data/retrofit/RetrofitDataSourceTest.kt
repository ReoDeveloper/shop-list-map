package com.tiendeo.interviewtest.data.retrofit

import com.tiendeo.common.MapperProvider
import com.tiendeo.interviewtest.domain.model.Shop
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class RetrofitDataSourceTest : MockWebServerTest() {
    companion object {
        const val ANY_NAME = "Costa Coffee Palau del Mar"
        const val ANY_RET_NAME = "Costa Coffee"
        const val ANY_LAT = 41.38114
        const val ANY_LON = 2.185705
        const val ANY_ID: Long = 187529
        const val ANY_RET_ID: Long = 2967
        const val ANY_STREET = "Plaça de Pau Vila 1"
        const val ANY_CITY = "Barcelona"
        const val ANY_POSTAL = "08039"
        const val ANY_WEB = "http://www.costacoffee.es/"
        const val EMPTY_STRING = ""
        const val FALSE = false
    }

    private lateinit var apiClient: RetrofitDataSource

    @Before
    override fun setUp() {
        super.setUp()
        val mockWebServerEndpoint = baseEndpoint
        apiClient = RetrofitDataSource(MapperProvider.provideApiShopMapper(), mockWebServerEndpoint)
    }

    @Test
    fun sendsGetCharactersToTheCorrectEndpoint() {
        enqueueMockResponse(200, "getAllShops.json")

        apiClient.getAll()

        assertGetRequestSentTo("/shops.json")
    }

    @Test
    fun parsesShopsAndGetsAllOfThem() {
        enqueueMockResponse(200, "getAllShops.json")

        val result = apiClient.getAll()

        assertEquals(100, result.size)
        assertShopContainsExpectedValues(result[0])
    }


    @Test(expected = UnsupportedOperationException::class)
    fun throwsUnsupportedOperationExceptionOnStoreItem() {
        enqueueMockResponse(200)

        apiClient.store(givenAnyShop())
    }

    private fun assertShopContainsExpectedValues(item: Shop) {
        assertEquals(ANY_ID, item.shopId)
        assertEquals(ANY_NAME, item.shopName)
        assertEquals(ANY_LAT, item.latitude)
        assertEquals(ANY_LON, item.longitude)
        assertEquals(ANY_STREET, item.addressStreet)
        assertEquals(ANY_CITY, item.addressCity)
        assertEquals(ANY_POSTAL, item.addressPostalCode)
        assertEquals(ANY_RET_ID, item.retailerId)
        assertEquals(ANY_RET_NAME, item.retailerName)
        assertEquals(FALSE, item.isHasCatalogs)
        assertEquals(EMPTY_STRING, item.phoneNumber)
        assertEquals(ANY_WEB, item.web)
    }

    private fun givenAnyShop(): Shop {

        return Shop(
                ANY_ID,
                ANY_NAME,
                ANY_LAT,
                ANY_LON,
                ANY_STREET,
                ANY_CITY,
                ANY_POSTAL,
                ANY_RET_ID,
                ANY_RET_NAME,
                FALSE,
                EMPTY_STRING,
                ANY_WEB)
    }
}