package com.tiendeo.interviewtest.data.retrofit

import junit.framework.Assert.assertEquals
import org.junit.Test

class RfShopMapperTest {

    private val mapper = RfShopMapper()

    companion object {
        const val ANY_NAME = "Costa Coffee Palau del Mar"
        const val ANY_RET_NAME = "Costa Coffee"
        const val ANY_LAT = 41.38114
        const val ANY_API_LAT = "41,38114"
        const val ANY_LON = 2.185705
        const val ANY_API_LON = "2,185705"
        const val ANY_ID: Long = 187529
        const val ANY_API_ID = "187529"
        const val ANY_RET_ID: Long = 2967
        const val ANY_API_RET_ID = "2967"
        const val ANY_STREET = "Plaça de Pau Vila 1"
        const val ANY_CITY = "Barcelona"
        const val ANY_POSTAL = "08080"
        const val ANY_PHONE = "600000000"
        const val ANY_WEB = "http://www.costacoffee.es/"
        const val EMPTY_STRING = ""
        const val FALSE = false
        const val TRUE = true
        const val ANY_HAS = FALSE
    }

    @Test fun shouldReturnShopWithId() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_ID, test.shopId)
    }

    @Test fun shouldReturnShopWithName() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_NAME, test.shopName)
    }

    @Test fun shouldReturnShopWithLatitude() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_LAT, test.latitude)
    }

    @Test fun shouldReturnShopWithLongitude() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_LON, test.longitude)
    }

    @Test fun shouldReturnShopWithStreet() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_STREET, test.addressStreet)
    }

    @Test fun shouldReturnShopWithCity() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_CITY, test.addressCity)
    }

    @Test fun shouldReturnShopWithPostal() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_POSTAL, test.addressPostalCode)
    }

    @Test fun shouldReturnShopWithRetailerId() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_RET_ID, test.retailerId)
    }

    @Test fun shouldReturnShopWithRetailerName() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_RET_NAME, test.retailerName)
    }

    @Test fun shouldReturnShopWithNoCatalogs() {
        val given = givenValidApiShopNoCatalogs()

        val test = mapper.map(given)

        assertEquals(FALSE, test.isHasCatalogs)
    }
    @Test fun shouldReturnShopWithCatalogs() {
        val given = givenValidApiShopHasCatalogs()

        val test = mapper.map(given)

        assertEquals(TRUE, test.isHasCatalogs)
    }

    @Test fun shouldReturnShopWithPhone() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_PHONE, test.phoneNumber)
    }

    @Test fun shouldReturnShopWithWeb() {
        val given = givenValidApiShop()

        val test = mapper.map(given)

        assertEquals(ANY_WEB, test.web)
    }

    @Test fun shouldReturnShopWithNoWeb() {
        val given = givenValidApiShopNoWeb()

        val test = mapper.map(given)

        assertEquals(EMPTY_STRING, test.web)
    }

    private fun givenValidApiShop() = ApiShop(ANY_API_ID, ANY_NAME, ANY_API_LAT, ANY_API_LON,
            ANY_STREET, ANY_CITY, ANY_POSTAL, ANY_API_RET_ID, ANY_RET_NAME, ANY_HAS, ANY_PHONE, ANY_WEB)
    private fun givenValidApiShopHasCatalogs() = ApiShop(ANY_API_ID, ANY_NAME, ANY_API_LAT, ANY_API_LON,
            ANY_STREET, ANY_CITY, ANY_POSTAL, ANY_API_RET_ID, ANY_RET_NAME, TRUE, ANY_PHONE, ANY_WEB)
    private fun givenValidApiShopNoCatalogs() = ApiShop(ANY_API_ID, ANY_NAME, ANY_API_LAT, ANY_API_LON,
            ANY_STREET, ANY_CITY, ANY_POSTAL, ANY_API_RET_ID, ANY_RET_NAME, FALSE, ANY_PHONE, ANY_WEB)
    private fun givenValidApiShopNoWeb() = ApiShop(ANY_API_ID, ANY_NAME, ANY_API_LAT, ANY_API_LON,
            ANY_STREET, ANY_CITY, ANY_POSTAL, ANY_API_RET_ID, ANY_RET_NAME, ANY_HAS, ANY_PHONE, EMPTY_STRING)
}